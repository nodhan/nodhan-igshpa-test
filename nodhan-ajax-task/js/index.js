$(".btn").click(function() {
    /**
     * Write your code here
     */
    $(this).prop("disabled", true);
    $(this).prop("value", "Loading...");
    $(".panel-body").addClass("whirl duo");
    $.ajax({
        url: "https://qrcode.igshpa.org/api-status",
        method: "GET",
        success: function(data) {
            var toAppend = "";
            var started = moment(new Date(data.started));
            var seconds = parseFloat(data.uptime);

            var hours = Math.floor(seconds / 3600);

            var upTime = hours + " Hour";
            if (hours !== 1) {
                upTime += "s";
            }
            seconds -= hours * 3600;
            var minutes = Math.floor(seconds / 60);
            if (minutes > 0) {
                upTime += " and " + minutes + " Minute";
                if (minutes !== 1) {
                    upTime += "s";
                }
            }

            toAppend =
                "<div class='row'><div class='col-md-4'>" +
                started.format("MMMM DD, YYYY HH:mm") +
                "</div><div class='col-md-4'>" +
                upTime +
                "</div><div class='col-md-4'>" +
                started
                    .add({ hours: hours, minutes: minutes })
                    .format("MMMM DD, YYYY HH:mm") +
                "</div></div>";
            $(".panel-body").append(toAppend);
        }
    });
    $(".panel-body").removeClass("whirl duo");
    $(this).prop("disabled", false);
    $(this).prop("value", "Click me!");
});
